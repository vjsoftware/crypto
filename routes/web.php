<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/topup', 'TopupController@index')->name('topupv');
Route::post('/topup', 'TopupController@topup')->name('topuppost');
Route::get('/topup-success', 'TopupController@success')->name('topupsuccess');
Route::get('/account', function () {
  return view('auth.account');
});
Route::get('/success', function () {
  return view('auth.success');
});
Route::get('/register-pay', 'PaymentsController@firstpayview')->name('firstpayview');
Route::post('/register-pay', 'PaymentsController@firstpaypost')->name('firstpay');
// Route::get('/success', 'HomeController@success')->name('success');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');
