<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Investment;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $currentUser = Auth::user()->id;
        $investments = User::find($currentUser)->investments;
        $payments = User::find($currentUser)->payments;
        $data = [
          'investments' => $investments,
          'payments' => $payments
        ];
        return view('home')->with('data', $data);
    }

    // After register Success View
    public function success()
    {
        return view('success');
    }
}
