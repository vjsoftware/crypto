<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class PaymentsController extends Controller
{

    // First pay View
    public function firstpayview()
    {
      return view('auth.account');
    }

    // First Pay POST Form
    public function firstpaypost(Request $request)
    {
      // return $request->amount;
      if ($request->hasFile('screenshot')) {

          $file = $request->screenshot;
          // return $file;
          // $img = Image::make($file->getRealPath());
          // $fileExt1 = $file->getClientOriginalExtension();
          // $fullPhotoNameWithExt1 = $file->getClientOriginalName();
          // $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
          // $photoToSave1 = "listings/".$fileName1.'_'.time().'.'. $fileExt1;
          // // $url =
          // $filename = $this->compress_image($_FILES["cover"]["tmp_name"], $photoToSave1, 80);
          // $buffer = file_get_contents($photoToSave1);
          $imageFileName = time() . '.' . $file->getClientOriginalExtension();
          $location = env('AWS_DEFAULT_REGION');
          $bucket = env('AWS_BUCKET');
          $filePath = '/screenshots/' . $imageFileName;
          $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
          $s3 = \Storage::disk('s3');
          $filePath = '/screenshots/' . $imageFileName;
          $s3->put($filePath, file_get_contents($file), 'public');
          $photoToSave1 = $imageFileNameFull;
        }else {
          $photoToSave1 = ' ';
        }

        $userId = $request->user_id;
        // $updateUser = User::where('email', $userId)->get();
        $updateUser = DB::UPDATE("UPDATE `users` SET `amount` = '$request->amount', `tnxid` = '$request->tnxid', `pic` = '$photoToSave1' WHERE `email` = '$userId'");
        // $updateUser->amount = $request->amount;
        // $updateUser->tnxid = $request->tnxid;
        // $updateUser->pic = $request->$photoToSave1;
        // $updateUser->save();
        return redirect('/success');
    }
}
