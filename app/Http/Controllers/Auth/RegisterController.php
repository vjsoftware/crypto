<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Referal;
use App\Smref;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use DB;
use Session;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect('/register-pay');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $newUser = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'emailnew' => $data['emailnew'],
            'password' => Hash::make($data['password']),
            'mobile' => $data['mobile'],
            'gender' => $data['gender'],
            'bank_name' => $data['bank_name'],
            'account_holder_name' => $data['account_holder_name'],
            'account_no' => $data['account_no'],
            'ifsc' => $data['ifsc'],
            'referal_id' => $data['referal_id'],
        ]);

        if (substr($data['referal_id'], 0, 2) == 'sm') {
          $newSmRef = new Smref();
          $newSmRef->sm_id = $data['referal_id'];
          $newSmRef->user_id = $data['email'];
          $newSmRef->amount = '10';
          $newSmRef->save();
        }

        $findSm = Smref::where('user_id', $data['referal_id'])->get();

        if (count($findSm) > 0) {
          $smId = $findSm[0]->sm_id;
          $link = $findSm[0]->user_id;
          $newSmRef = new Smref();
          $newSmRef->sm_id = $smId;
          $newSmRef->user_id = $data['email'];
          $newSmRef->link = $link;
          $newSmRef->amount = '10';
          $newSmRef->save();
        }

        

        $currentUserId = $data['email'] + 1;
        $newCount = str_pad($currentUserId, 4, '0', STR_PAD_LEFT);
        $UserIdCountUpdate = DB::UPDATE("UPDATE `counts` set `user_id` = '$newCount' WHERE `id` = 1");
        $sessionStoreId = $data['email'];
        Session::put('user_id', $sessionStoreId);
        return $newUser;
    }
}
