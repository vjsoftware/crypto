<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topup;

use Auth;

class TopupController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
    return View('topup');
  }

  public function topup(Request $request)
  {
    $userId = Auth::user()->id;
    // return $userId;
    // return $request;
    if ($request->hasFile('screenshot')) {

        $file = $request->screenshot;
        $imageFileName = time() . '.' . $file->getClientOriginalExtension();
        $location = env('AWS_DEFAULT_REGION');
        $bucket = env('AWS_BUCKET');
        $filePath = '/investments/' . $imageFileName;
        $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
        $s3 = \Storage::disk('s3');
        $filePath = '/investments/' . $imageFileName;
        $s3->put($filePath, file_get_contents($file), 'public');
        $photoToSave1 = $imageFileNameFull;
      }else {
        $photoToSave1 = ' ';
      }



      $newTopup = new Topup();
      $newTopup->user_id = $userId;
      $newTopup->amount = $request->amount;
      $newTopup->tnxid = $request->tnxid;
      $newTopup->pic = $photoToSave1;
      $newTopup->status = '0';
      $newTopup->save();

      return redirect('/topup-success');

  }

  public function success()
  {
    return View('invest-success');
  }
}
