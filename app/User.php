<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

use App\Investment;
use App\Payment;
use App\Topup;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'emailnew', 'password', 'mobile', 'gender', 'account_holder_name', 'account_no', 'bank_name', 'ifsc', 'referal_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the Investments.
     */
    public function investments()
    {
        return $this->hasMany(Investment::class);
    }
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
    public function topups()
    {
        return $this->hasMany(Topup::class);
    }
}
