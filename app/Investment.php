<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Investment extends Model
{
  /**
   * Get the User.
   */
  public function user()
  {
      return $this->belongsTo(User::class);
  }
}
