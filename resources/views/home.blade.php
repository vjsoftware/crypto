@php
  use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">
   <!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:07 GMT -->
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="https://bootstraptemplatedesign.com/website/Adminux/favicon.ico">
      <title>Zorgen Dashboard</title>
      <!-- Fontawesome icon CSS -->
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{ config('app.url') }}/ux/vendor/bootstrap-4.1.1/css/bootstrap.css" type="text/css">
      <!-- DataTables Responsive CSS -->
      <link href="{{ config('app.url') }}/ux/vendor/datatables/css/dataTables.bootstrap4.css" rel="stylesheet">
      <link href="{{ config('app.url') }}/ux/vendor/datatables/css/responsive.dataTables.min.css" rel="stylesheet">
      <!-- jvectormap CSS -->
      <link href="{{ config('app.url') }}/ux/vendor/jquery-jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet">
      <!-- Adminux CSS -->
      <link rel="stylesheet" href="{{ config('app.url') }}/ux/css/light_adminux.css" type="text/css">
   </head>
   <body class="menuclose menuclose-right">
      <!-- Page Loader -->
      <div class="loader_wrapper align-items-center text-center">
         <div class="load7 load-wrapper">
            <img src="{{ config('app.url') }}/ux/img/logo.png" alt="" class="loading_img">
            <div class="loader"> Loading... </div>
            <div class="clearfix"></div>
            <br>
            <br>
            <br>
            <br>
            <h4 class="text-white">Petal of Flower</h4>
            <p>Awesome things are getting ready...</p>
         </div>
      </div>
      <!-- Page Loader Ends -->
      @include('layouts.navbar')
      @include('layouts.sidebar')
      <div class="wrapper-content">
         <div class="container">
            <div class="row  align-items-center justify-content-between">
               <div class="col-11 col-sm-12 page-title">
                  <h3>Zorgen Dashboard</h3>
               </div>
            </div>
            {{-- Stats Start --}}
            <div class="row">
               <div class="col-md-8 col-lg-8 col-xl-4">
                  <div class="activity-block success">
                     <div class="media">
                        <div class="media-body">
                           @php
                             $calculateTotalInvestments = 0;
                             foreach ($data['investments'] as $investment) {
                             $calculateTotalInvestments += $investment->amount;
                           }
                           @endphp
                           <h5>₹ <span class="spincreament">{{ $calculateTotalInvestments }}</span></h5>
                           <p>Total Investment</p>
                        </div>
                        <img src="{{ config('app.url') }}/ux/icons/rupee.svg" alt="" style="height: 25px; color: #fff;">
                     </div>
                     <br>
                     {{-- <div class="media">
                        <div class="media-body"><span class="progress-heading">Your Total Investment</span></div>
                     </div> --}}
                     <div class="row">
                        <div class="progress ">
                           <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 70%;"><span class="trackerball"></span></div>
                        </div>
                     </div>
                     <i class="bg-icon text-center fa fa-cubes"></i>
                  </div>
               </div>
               <div class="col-md-8 col-lg-8 col-xl-4">
                  <div class="activity-block danger">
                     <div class="media">
                        <div class="media-body">
                          @php
                          $totalProfit = 0;
                          // foreach ($investments as $investment) {
                          //
                          //   $interest = (($investment->amount / 100) * 2);
                          //   // dd($interest);
                          //
                          //   $created = new Carbon($investment->created_at);
                          //   $now = Carbon::now();
                          //   $difference = ($created->diff($now)->days < 1)
                          //   ? '1'
                          //   : $created->diffInDays($now);
                          //
                          //   $totalProfit += $difference * $interest;
                          // }
                            // dd($difference);
                            foreach ($data['payments'] as $payment) {
                              $totalProfit += $payment['amount'];
                            }
                          @endphp
                           <h5>₹ <span class="spincreament">{{ $totalProfit }}</span></h5>
                           <p>Income Generated</p>
                        </div>
                        <img src="{{ config('app.url') }}/ux/icons/balance.svg" alt="" style="height: 25px; color: #fff;">
                     </div>
                     <br>
                     <div class="row">
                        <div class="progress ">
                           <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"><span class="trackerball"></span></div>
                        </div>
                     </div>
                     <i class="bg-icon text-center fa fa-bar-chart-o"></i>
                  </div>
               </div>
               <div class="col-md-8 col-lg-8 col-xl-4">
                  <div class="activity-block warning">
                     <div class="media">
                        <div class="media-body">
                          @php
                            $currentUser = Auth::user()->email;
                            $referalSum = DB::SELECT("SELECT SUM(amount) AS referalsum FROM `referals` WHERE `user_id` = '$currentUser'");
                          @endphp
                           <h5>₹ <span class="spincreament">{{ $referalSum[0]->referalsum }}</span></h5>
                           <p>Referal Income</p>
                        </div>
                        <img src="{{ config('app.url') }}/ux/icons/users.svg" alt="" style="height: 25px; color: #fff;">
                     </div>
                     <br>
                     <div class="row">
                        <div class="progress ">
                           <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 45%;"><span class="trackerball"></span></div>
                        </div>
                     </div>
                     <i class="bg-icon text-center fa fa-users"></i>
                  </div>
               </div>
               <div class="col-md-8 col-lg-8 col-xl-4">
                  <div class="activity-block primary">
                     <div class="media">
                        <div class="media-body">
                           <h5><span class="spincreament">{{ $totalProfit + 0 }}</span></h5>
                           <p>Paid Amount</p>
                        </div>
                        <img src="{{ config('app.url') }}/ux/icons/wallet.svg" alt="" style="height: 25px; color: #fff;">
                     </div>
                     <br>
                     <div class="row">
                        <div class="progress ">
                           <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"><span class="trackerball"></span></div>
                        </div>
                     </div>
                     <i class="bg-icon text-center fa fa-money"></i>
                  </div>
               </div>
            </div>
            {{-- Stats End --}}
            <div class="row">
               <div class="col-sm-16">
                  <div class="card">
                     <div class="card-header">
                        <h5 class="card-title">Investment's <small>Report</small></h5>
                     </div>
                     <div class="card-body">
                        <table class="table " id="dataTables-example">
                           <thead>
                              <tr>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Days</th>
                                <th>Income</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($data['investments'] as $investment)
                                <tr class="odd">
                                  <td class="center">{{ $investment->amount }}</td>
                                  @php
                                    $formatDate = Carbon::createFromFormat('Y-m-d H:i:s', $investment->created_at)->format('d-m-Y');
                                  @endphp
                                  <td class="center">{{ $formatDate }}</td>
                                  @php
                                  $totalProfit = 0;


                                    // $interest = (($investment->amount / 100) * 2);
                                    // dd($interest);

                                    $created = new Carbon($investment->created_at);
                                    $now = Carbon::now();
                                    $difference = ($created->diff($now)->days < 1)
                                    ? '1'
                                    : $created->diffInDays($now);

                                    // $totalProfit += $difference * $interest;
                                    // dd($difference);
                                    $totalProfit = DB::SELECT("SELECT SUM(amount) AS totalProfit FROM `payments` WHERE `investment_id` = '$investment->id'");
                                  @endphp
                                  <td class="center"><span class="badge badge-success">{{ $difference }}</span></td>
                                  <td class="center">{{ $totalProfit[0]->totalProfit }}</td>
                                </tr>
                              @endforeach

                           </tbody>
                        </table>
                        <!-- /.table-responsive -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         @include('layouts.footer')
      </div>
      <!-- themepicker modal ends here ! -->
      <!-- jQuery first, then Tether, then Bootstrap JS. -->
      <script src="{{ config('app.url') }}/ux/js/jquery-2.1.1.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/ux/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="{{ config('app.url') }}/ux/vendor/bootstrap4beta/js/bootstrap.min.js" type="text/javascript"></script>
      <!--Cookie js for theme chooser and applying it -->
      <script src="{{ config('app.url') }}/ux/vendor/cookie/jquery.cookie.js"  type="text/javascript"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="{{ config('app.url') }}/ux/js/ie10-viewport-bug-workaround.js"></script>
      <!-- Circular chart progress js -->
      <script src="{{ config('app.url') }}/ux/vendor/cicular_progress/circle-progress.min.js" type="text/javascript"></script>
      <!--sparklines js-->
      <script type="text/javascript" src="{{ config('app.url') }}/ux/vendor/sparklines/jquery.sparkline.min.js"></script>
      <!-- jvectormap JavaScript -->
      <script src="{{ config('app.url') }}/ux/vendor/jquery-jvectormap/jquery-jvectormap.js"></script>
      <script src="{{ config('app.url') }}/ux/vendor/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
      <!-- chart js -->
      <script src="{{ config('app.url') }}/ux/vendor/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/ux/vendor/chartjs/utils.js" type="text/javascript"></script>
      <!-- spincremente js -->
      <script src="{{ config('app.url') }}/ux/vendor/spincrement/jquery.spincrement.min.js" type="text/javascript"></script>
      <!-- DataTables JavaScript -->
      <script src="{{ config('app.url') }}/ux/vendor/datatables/js/jquery.dataTables.min.js"></script>
      <script src="{{ config('app.url') }}/ux/vendor/datatables/js/dataTables.bootstrap4.js"></script>
      <script src="{{ config('app.url') }}/ux/vendor/datatables/js/dataTables.responsive.min.js"></script>
      <!-- custome template js -->
      <script src="{{ config('app.url') }}/ux/js/adminux.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/ux/js/dashboard1.js"></script>
   </body>
   <!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:50 GMT -->
</html>
