@php
use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">
   <!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:07 GMT -->
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="https://bootstraptemplatedesign.com/website/Adminux/favicon.ico">
      <title>Zorgen Dashboard</title>
      <!-- Fontawesome icon CSS -->
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{ config('app.url') }}/ux/vendor/bootstrap-4.1.1/css/bootstrap.css" type="text/css">
      <!-- DataTables Responsive CSS -->
      <link href="{{ config('app.url') }}/ux/vendor/datatables/css/dataTables.bootstrap4.css" rel="stylesheet">
      <link href="{{ config('app.url') }}/ux/vendor/datatables/css/responsive.dataTables.min.css" rel="stylesheet">
      <!-- jvectormap CSS -->
      <link href="{{ config('app.url') }}/ux/vendor/jquery-jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet">
      <!-- Adminux CSS -->
      <link rel="stylesheet" href="{{ config('app.url') }}/ux/css/light_adminux.css" type="text/css">
   </head>
   <body class="menuclose menuclose-right">
      <!-- Page Loader -->
      <div class="loader_wrapper align-items-center text-center">
         <div class="load7 load-wrapper">
            <img src="{{ config('app.url') }}/ux/img/logo.png" alt="" class="loading_img">
            <div class="loader"> Loading... </div>
            <div class="clearfix"></div>
            <br>
            <br>
            <br>
            <br>
            <h4 class="text-white">Petal of Flower</h4>
            <p>Awesome things are getting ready...</p>
         </div>
      </div>
      <!-- Page Loader Ends -->
      @include('layouts.navbar')
      @include('layouts.sidebar')
      <div class="wrapper-content">
         <div class="container">
            <div class="row  align-items-center justify-content-between">
               <div class="col-11 col-sm-12 page-title">
                  <h3>Zorgen TopUp Request Panel</h3>
               </div>
            </div>
            {{-- Stats Start --}}
            <div class="row">
              <div class="col-sm-16">
                <div class="card">
                  <div class="card-header">
                    <h6 class="card-title">Request Status</h6>
                  </div>
                  <div class="card-body">
                    <h5>We received your request</h5>
                    <h5>Your account will be updated in 2 Working Hours</h5>
                  </div>
                </div>
              </div>
            </div>
            {{-- Stats End --}}
         </div>
         @include('layouts.footer')
      </div>
      <!-- themepicker modal ends here ! -->
      <!-- jQuery first, then Tether, then Bootstrap JS. -->
      <script src="{{ config('app.url') }}/ux/js/jquery-2.1.1.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/ux/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="{{ config('app.url') }}/ux/vendor/bootstrap4beta/js/bootstrap.min.js" type="text/javascript"></script>
      <!--Cookie js for theme chooser and applying it -->
      <script src="{{ config('app.url') }}/ux/vendor/cookie/jquery.cookie.js"  type="text/javascript"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="{{ config('app.url') }}/ux/js/ie10-viewport-bug-workaround.js"></script>
      <!-- Circular chart progress js -->
      <script src="{{ config('app.url') }}/ux/vendor/cicular_progress/circle-progress.min.js" type="text/javascript"></script>
      <!--sparklines js-->
      <script type="text/javascript" src="{{ config('app.url') }}/ux/vendor/sparklines/jquery.sparkline.min.js"></script>
      <!-- jvectormap JavaScript -->
      <script src="{{ config('app.url') }}/ux/vendor/jquery-jvectormap/jquery-jvectormap.js"></script>
      <script src="{{ config('app.url') }}/ux/vendor/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
      <!-- chart js -->
      <script src="{{ config('app.url') }}/ux/vendor/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/ux/vendor/chartjs/utils.js" type="text/javascript"></script>
      <!-- spincremente js -->
      <script src="{{ config('app.url') }}/ux/vendor/spincrement/jquery.spincrement.min.js" type="text/javascript"></script>
      <!-- DataTables JavaScript -->
      <script src="{{ config('app.url') }}/ux/vendor/datatables/js/jquery.dataTables.min.js"></script>
      <script src="{{ config('app.url') }}/ux/vendor/datatables/js/dataTables.bootstrap4.js"></script>
      <script src="{{ config('app.url') }}/ux/vendor/datatables/js/dataTables.responsive.min.js"></script>
      <!-- custome template js -->
      <script src="{{ config('app.url') }}/ux/js/adminux.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/ux/js/dashboard1.js"></script>
   </body>
   <!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:50 GMT -->
</html>
