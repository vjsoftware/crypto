<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/sign-in2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:53 GMT -->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="https://bootstraptemplatedesign.com/website/Adminux/favicon.ico">
<title>Zorgen Login</title>
<!-- Fontawesome icon CSS -->
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ config('app.url') }}/ux/vendor/bootstrap-4.1.1/css/bootstrap.css" type="text/css">

<!-- Adminux CSS -->
 <link rel="stylesheet" href="{{ config('app.url') }}/ux/css/dark_blue_adminux.css" type="text/css">
</head>
<body class="menuclose menuclose-right">
<!-- Page Loader -->
<div class="loader_wrapper inner align-items-center text-center">
  <div class="load7 load-wrapper">
    <div class="loading_img"></div>
    <div class="loader"> Loading... </div>
    <div class="clearfix"></div>
  </div>
</div>
<!-- Page Loader Ends -->


<header class="navbar-fixed">
<nav class="navbar navbar-toggleable-md sign-in-header">
  {{-- <div class="sidebar-left">  <a class="navbar-brand imglogo" href="index.html"></a> </div> --}}
  <div class="col"></div>
  <div class="sidebar-right pull-right" >
    <ul class="navbar-nav  justify-content-end">
      {{-- <li><a href="#" class="btn btn-link text-white" >Need Help ?</a></li> --}}
      <li><a href="{{ route('register') }}" class="btn btn-primary " >Register</a></li>
    </ul>
  </div>
</nav>
</header>
<div class="wrapper-content-sign-in ">
  <div class="container text-center">
    <h2 class="display-4 form-signin-heading text-white"><i class="fa fa-trophy"></i> Zorgen</h2>

    <form class="form-signin1 white" method="POST" action="{{ route('login') }}">
      @csrf
      <h2 class="tex-black mb-4">Sign in</h2>
      <label  class="">{{ __('User ID') }}</label>
      <div class="input-group">
        <span class="input-group-addon" style="padding: 13px"><i class="fa fa-user"></i></span>
        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="User ID">
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
      </div>
      <br>
      <label class="">{{ __('Password') }}</label>
      <div class="input-group">
        <span class="input-group-addon" style="padding: 10px"><i class="fa fa-key"></i></span>
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
      <br>
      <div class="checkbox">
        <label class="form-check-label active">
          <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
          <i class="fa fa-check"></i></label>
        {{ __('Remember Me') }} </div>
      {{-- <a href="index.html" class="btn btn-lg btn-primary btn-round">Sign in</a><br> --}}
      <input type="submit" name="singin" value="Sign In" class="btn btn-lg btn-primary btn-round">
      <br>
      {{-- <a href="#" class="btn btn-link mt-2 ">Forgot password?</a> --}}
    </form>
    <p class="mt-3">Dont have account yet? <a href="{{ route('register') }}" class="text-white">Signup here</a>!</p>
  </div>
</div>


<!-- jQuery first, then Tether, then Bootstrap JS. -->

<script src="{{ config('app.url') }}/ux/js/jquery-2.1.1.min.js" type="text/javascript"></script>

<script src="{{ config('app.url') }}/ux/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

<script src="{{ config('app.url') }}/ux/vendor/bootstrap4beta/js/bootstrap.min.js" type="text/javascript"></script>

<!--Cookie js for theme chooser and applying it -->
<script src="{{ config('app.url') }}/ux/vendor/cookie/jquery.cookie.js"  type="text/javascript"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> <script src="{{ config('app.url') }}/ux/js/ie10-viewport-bug-workaround.js"></script> <script>
            "use strict";
            $('input[type="checkbox"]').on('change', function(){
                $(this).parent().toggleClass("active")
                $(this).closest(".media").toggleClass("active");
            });
        $(window).on("load", function(){
            /* loading screen */
            $(".loader_wrapper").fadeOut("slow");
        });
        </script>
</body>

<!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/sign-in2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:53 GMT -->
</html>
