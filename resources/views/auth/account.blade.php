<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/sign-in2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:53 GMT -->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="https://bootstraptemplatedesign.com/website/Adminux/favicon.ico">
<title>Zorgen Register</title>
<!-- Fontawesome icon CSS -->
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ config('app.url') }}/ux/vendor/bootstrap-4.1.1/css/bootstrap.css" type="text/css">

<!-- Adminux CSS -->
 <link rel="stylesheet" href="{{ config('app.url') }}/ux/css/dark_blue_adminux.css" type="text/css">
</head>
<body class="menuclose menuclose-right">
<!-- Page Loader -->
<div class="loader_wrapper inner align-items-center text-center">
  <div class="load7 load-wrapper">
    <div class="loading_img"></div>
    <div class="loader"> Loading... </div>
    <div class="clearfix"></div>
  </div>
</div>
<!-- Page Loader Ends -->


<header class="navbar-fixed">
<nav class="navbar navbar-toggleable-md sign-in-header">
  {{-- <div class="sidebar-left">  <a class="navbar-brand imglogo" href="index.html"></a> </div> --}}
  <div class="col"></div>
  <div class="sidebar-right pull-right" >
    <ul class="navbar-nav  justify-content-end">
      {{-- <li><a href="#" class="btn btn-link text-white" >Need Help ?</a></li> --}}
      <li><a href="{{ route('login') }}" class="btn btn-primary " >Login</a></li>
    </ul>
  </div>
</nav>
</header>
<div class="wrapper-content-sign-in ">
  <div class="container text-center">
    <h2 class="display-4 form-signin-heading text-white"><i class="fa fa-trophy"></i> Zorgen</h2>

    <form class="form-signin1 white" method="POST" action="{{ route('firstpay') }}" style="max-width: 820px;" enctype="multipart/form-data">
      @csrf
      <p for="" class="text-danger text-center text-justify">{{ __('Bank Details') }}</p>
      <div class="form-group row">
          <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Account Name: ') }}</label>

          <label for="name" class="col-md-4 col-form-label text-primary">{{ __('ZORGENTRADERS') }}</label>
      </div>
      <div class="form-group row">
          <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Account Number: ') }}</label>

          <label for="name" class="col-md-4 col-form-label text-primary">{{ __('800800327861') }}</label>
      </div>
      <div class="form-group row">
          <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('IFSC: ') }}</label>

          <label for="name" class="col-md-4 col-form-label text-primary">{{ __('YESB0CMSNOC') }}</label>
          <label for="name" class="col-md-4 col-form-label text-danger" style="font-size: 12px;">{{ __('(*FIFTH LETTER ZERO)') }}</label>
      </div>

      {{-- <div class="form-group row">
          <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('User ID') }}</label>
          @php
            $getUserId = DB::SELECT("SELECT * FROM `counts` WHERE `id` = 1");
          @endphp
          <div class="col-md-12">
            <input disabled id="email" type="number" class="form-control @error('email') is-invalid @enderror" value="{{ $getUserId[0]->user_id }}" required autocomplete="email">

              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div> --}}
      <div class="form-group row">
          <label for="emailnew" class="col-md-4 col-form-label text-md-right">{{ __('Amount') }}</label>

          <div class="col-md-12">
              <input type="hidden" name="user_id" value="{{ session()->get('user_id') }}" required>
              {{-- <input id="amount" type="text" class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}" required autocomplete="amount"> --}}
                <select class="form-control" name="amount" required autofocus>
                  <option value="10000">10000</option>
                  <option value="20000">20000</option>
                  <option value="30000">30000</option>
                  <option value="40000">40000</option>
                  <option value="50000">50000</option>
                  <option value="60000">60000</option>
                  <option value="70000">70000</option>
                  <option value="80000">80000</option>
                  <option value="90000">90000</option>
                  <option value="100000">100000</option>
                </select>
              @error('amount')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $amount }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row">
          <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Bank Transaction ID') }}</label>

          <div class="col-md-12">
              <input id="tnxid" type="text" class="form-control @error('tnxid') is-invalid @enderror" name="tnxid" required autocomplete="tnxid">


              @error('tnxid')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $tnxid }}</strong>
                  </span>
              @enderror
          </div>
      </div>
      <p for="" class="text-danger text-center text-justify">{{ __('Upload Bank Slip / Screen Shot') }}</p>
      <div class="form-group row">
          <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Picture') }}</label>

          <div class="col-md-12">
              <input id="screenshot" type="file" class="form-control" name="screenshot" required autocomplete="screenshot">
          </div>
      </div>


      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Register') }}
              </button>

          </div>
      </div>
    </form>
    <p class="mt-3">Already Have an Account? <a href="{{ route('login') }}" class="text-white">Login Here</a>!</p>
  </div>
</div>


<!-- jQuery first, then Tether, then Bootstrap JS. -->

<script src="{{ config('app.url') }}/ux/js/jquery-2.1.1.min.js" type="text/javascript"></script>

<script src="{{ config('app.url') }}/ux/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

<script src="{{ config('app.url') }}/ux/vendor/bootstrap4beta/js/bootstrap.min.js" type="text/javascript"></script>

<!--Cookie js for theme chooser and applying it -->
<script src="{{ config('app.url') }}/ux/vendor/cookie/jquery.cookie.js"  type="text/javascript"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> <script src="{{ config('app.url') }}/ux/js/ie10-viewport-bug-workaround.js"></script> <script>
            "use strict";
            $('input[type="checkbox"]').on('change', function(){
                $(this).parent().toggleClass("active")
                $(this).closest(".media").toggleClass("active");
            });
        $(window).on("load", function(){
            /* loading screen */
            $(".loader_wrapper").fadeOut("slow");
        });
        </script>
</body>

<!-- Mirrored from bootstraptemplatedesign.com/website/Adminux/pages/sign-in2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 25 Aug 2019 05:55:53 GMT -->
</html>
