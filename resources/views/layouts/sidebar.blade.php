<div class="sidebar-left">
  <div class="user-menu-items">
    <div class="list-unstyled btn-group">
      <button class="media btn btn-link dropdown-toggle"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="message_userpic"><img class="d-flex" src="{{ config('app.url') }}/ux/img/user-header.png" alt="Generic user image"></span> <span class="media-body"> <span class="mt-0 mb-1">{{ Auth::user()->email }}</span> <small>{{ Auth::user()->name }}.</small> </span> </button>
      <div class="dropdown-menu"> <a class="dropdown-item" href="customerprofile.html">Profile</a> <a class="dropdown-item" href="inbox.html">Mailbox</a> <a class="dropdown-item" href="#">Application</a> <a class="dropdown-item" href="#">Analytics Report</a> <a class="dropdown-item" href="#">Preferances</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Setting</a> </div>
    </div>
  </div>
  <br>

  {{-- Menu Start --}}
  <ul class="nav flex-column in" id="side-menu">
    <li class="title-nav">MENU</li>
    <li class=" nav-item"><a  href="{{  route('home')}}" class="nav-link "><i class="fa fa-globe"></i> Dashboard</a></li>
    <li class=" nav-item"><a  href="{{  route('topupv')}}" class="nav-link "><i class="fa fa-globe"></i> Top Up</a></li>
</ul>
{{-- Menu End --}}
  <hr>
  <ul class="nav flex-column in">
    <li class="nav-item ">
<div class="nav-link">
    <h5>Alert <span class="fa fa-bell-o pull-right"></span></h5>
    <span class="meeting-subject">Agenda: Team mnufacturing review meeting. Board Compulsory</span>
    <div class="">
      <button class="btn btn-outline-success btn-round mr-sm-2">Got IT</button>
    </div>
    </div>
  </li>
  </ul>
  <br>
  <br>
</div>
