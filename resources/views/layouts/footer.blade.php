@php
  use Carbon\Carbon;
@endphp
<footer class="footer-content ">
  <div class="container ">
    <div class="row align-items-center justify-content-between">
      <div class="col-md-16 col-lg-8 col-xl-8">All Rights Reserved by <a href="{{ config('app.url') }}" target="_blank" class="">Zorgen.trade</a></div>
      @php
      $created = new Carbon(Auth::user()->created_at);
      $now = Carbon::now();
      $formatedDate = Carbon::createFromFormat('Y-m-d H:s:i', Auth::user()->created_at)->format('d-m-Y');
      @endphp
      <div class="col-md-16 col-lg-8 col-xl-8 text-right"><h3>Join Date: ({{ $formatedDate }}) Days: ({{ $created->diffInDays($now) }})</h3></div>
    </div>
  </div>
</footer>
